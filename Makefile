nwjs-win32=nwjs-v0.55.0-win-ia32
nwjs-linux64=nwjs-v0.55.0-linux-x64

nwjs-win32-d=https://dl.nwjs.io/v0.55.0/$(nwjs-win32).zip
nwjs-linux64-d=https://dl.nwjs.io/v0.55.0/$(nwjs-linux64).tar.gz

win32-download-nwjs:
	mkdir -p nwjs-precompiled
	cd nwjs-precompiled && curl -L -O $(nwjs-win32-d)

linux64-download-nwjs:
	mkdir -p nwjs-precompiled
	cd nwjs-precompiled && curl -L -O $(nwjs-linux64-d)

win32-deploy-nwjs:
	mkdir -p build-nwjs/$(nwjs-win32)
	rm -rf: build-nwjs/$(nwjs-win32)
	cd ./nwjs-precompiled/ && unzip $(nwjs-win32).zip -d ../build-nwjs/
	cp ./nwjs-package.json ./build-nwjs/$(nwjs-win32)/package.json

linux64-deploy-nwjs:
	mkdir -p build-nwjs/$(nwjs-linux64)
	rm -rf build-nwjs/$(nwjs-linux64)
	tar xvf ./nwjs-precompiled/$(nwjs-linux64).tar.gz -C build-nwjs/
	cp ./nwjs-package.json ./build-nwjs/$(nwjs-linux64)/package.json

win32-build-mdbook:
	mdbook build -d ./build-nwjs/$(nwjs-win32)/curso

linux64-build-mdbook:
	mdbook build -d ./build-nwjs/$(nwjs-linux64)/curso

win32-release-curse:
	mkdir -p release
	cd ./build-nwjs/$(nwjs-win32)/ && zip -r ./curso-rural-mi-primer-videojuego-win32.zip ./* && mv ./curso-rural-mi-primer-videojuego-win32.zip ../../release/ 

linux64-release-curse:
	mkdir -p release
	cd ./build-nwjs/$(nwjs-linux64)/ && tar czvf ../../release/curso-rural-mi-primer-videojuego-linux64.tar.gz ./* 

serve:
	mdbook serve
